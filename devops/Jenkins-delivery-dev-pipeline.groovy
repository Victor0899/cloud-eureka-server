node
{
	def mvnHome
	stage('Preparation')
	{
		try{
			git url: 'https://gitlab.com/Victor0899/cloud-eureka-server.git', branch: 'master'
			mvnHome = tool 'M2'
		}
		catch(e)
		{
			notifyStarted('Error al clonar proyecto Eureka-server de GitLab')
			throw e
		}
	}
	
	stage('Test')
	{
		try{
			sh "'${mvnHome}/bin/mvn' test"
		}
		catch(e)
		{
			notifyStarted("Error al realizar Test al proyecto Eureka-Server")
			throw e
		}
	}
	
	stage('Build')
	{
		try{
			sh "'${mvnHome}/bin/mvn' clean package -DskipsTests"
		}
		catch(e)
		{
			notifyStarted("Error al construir el proyecto Eureka-Server")
			throw e
		}
	}
	
	stage('Packaging')
	{
		try
		{
			archive 'target/*.jar'
		}
		catch(e)
		{
			notifyStarted("Error al empaquetar .JAR del proyecto Eureka-Server")
			throw e
		}	
		
	}
	
	stage('Deployment') {
        try{
            sh 'C:/Program Files (x86)/Jenkins/workspace/Microservices/Eureka-Server-DEV/runDeployment.sh'
        } catch (e) {
            notifyStarted("Error al desplegar el proyecto Eureka-Server")
            throw e
        }
    }
	
	stage('Notification')
	{
		notifyStarted("Tu codigo ha sido testeado y desplegado con exito!!")
	}
}


def notifyStarted(String message)
{
	slackSend (color: '#FFFF00', message: "${message}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
}